﻿
### ***Work Item Management***
----
The project purpose is to design and implement a Work Item Management (WIM) Console Application.

### ***Functional Requirements***

Application should support multiple teams. Each team has name, members and boards.

- ***Member*** has name, list of work items and activity history.

   - Name should be unique in the application

    - Name is a string between 5 and 15 symbols.

- ***Board*** has name, list of work items and activity history.

  - Name should be unique in the team

  - Name is a string between 5 and 10 symbols.

***There are 3 types of work items: bug, story and feedback.***

### **Bug**

Bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments and history.

- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Steps to reproduce is a list of strings.
- Priority is one of the following: High, Medium, Low
- Severity is one of the following: Critical, Major, Minor 
-  Status is one of the following: Active, Fixed 
-  Assignee is a member from the team.
- Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the bug.

### **Story**

Story has ID, title, description, priority, size, status, assignee, comments and history.
- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Priority is one of the following: High, Medium, Low
- Size is one of the following: Large, Medium, Small 
-  Status is one of the following: NotDone, InProgress, Done 
-  Assignee is a member from the team.
- Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the story.

### **Feedback**

Feedback has ID, title, description, rating, status, comments and history.

- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Rating is an integer.
- Status is one of the following: New, Unscheduled, Scheduled, Done 
- Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the feedback.

### **Operations**

Application should support the following operations:

- Create a new person 
- Show all people 
- Show person's activity 
- Create a new team 
- Show all teams
- Show team's activity
- Add person to team
-  Show all team members
-  Create a new board in a team
-  Show all team boards
-  Show board's activity
-  Create a new Bug/Story/Feedback in a board
-  Change Priority/Severity/Status of a bug
-  Change Priority/Size/Status of a story
-  Change Rating/Status of a feedback
-  Assign/Unassign work item to a person
-  Add comment to a work item


- List work items with options:

  - List all

  - Filter bugs/stories/feedback only

  - Filter by status and/or asignee

  - Sort by title/priority/severity/size/rating

## ***Commands***
**Create**
- CreateMember `[Name]` - creates a new Person with name _[Name]_ .
- CreateTeam `[TeamName]` - creates a new Team with name _[TeamName]_ .
- CreateBoard `[TeamName]` `[BoardName]`- creates a new Board with name _[BoardName]_ for team with name _[TeamName]_ .
- CreateBug `[PersonName]` `[BoardName]` `[TeamName]` `[BugName]` `[Description]` `[StepsToReproduce]` - creates a new Bug by specific person in team's board with _[Description]_ and _[StepsToReproduce]_ in the process.
- CreateStory `[PersonName]` `[BoardName]` `[TeamName]` `[StoryName]` `[Description]`  - creates a new Story by specific person in team's board with _[Description]_ .
- CreateFeedback `[PersonName]` `[BoardName]` `[TeamName]` `[FeedbackName]` `[Description]` `[Rating]` - creates a new Feedback by specific person in team's board with _[Description]_ and _[Rating]_  .

**Show**
- ShowAllTeams - shows all existing teams
- ShowAllTeamBoards `[TeamName]`- shows all boards of a specific team.
- ShowAllTeamMembers `[TeamName]` - shows all members a specific team.
- ShowAllPeople - shows all existing people
- ShowPeopleActivity `[PersonName]` - shows a given person's activity.
- ShowBoardActivity `[BoardName]` - shows a given board's activity.
- ShowTeamActivity `[TeamName]` - shows  a given team's activity .

**Add**
- AddPersonToTeam `[TeamName]` `[PersonName]` - adds an existing person to an existing team.
- AddCommentToWorkItem `[PersonName]` `[BoardName]` 	`[TeamName]` `[WorkItemName]` `[Message]` - lets a person who is part of a team's board to add a comment to a work item.

**Change**
- ChangePriority `[PersonName]` `[BoardName]` `[TeamName]` `[WorkItem]` `[Priority]` - lets a person who is part of a team's board change the priority of a work item.
- ChangeStatusOfBug `[PersonName]` `[BoardName]` `[TeamName]` `[BugName]` `[Status]` -lets a person who is part of a team's board change the status of a bug.
-  ChangeSeverityOfBug `[PersonName]` `[BoardName]` `[TeamName]` `[BugName]` `[Severity]` - lets a person who is part of a team's board change the severity of a bug.
- ChangeSizeOfStory `[PersonName]` `[BoardName]` `[TeamName]` `[StoryName]` `[Size]` - lets a person who is part of a team's board change the size of a story.
-  ChangeStatusOfStory `[PersonName]` `[BoardName]` `[TeamName]` `[StoryName]` `[Status]` - lets a person who is part of a team's board change the status of a story.
- ChangeRatingOfFeedback  `[PersonName]` `[BoardName]` `[TeamName]` `[FeedbackName]` `[Rating]` - lets a person who is part of a team's board change the rating of a feedback.
- ChangeStatusOfFeedback  `[PersonName]` `[BoardName]` `[TeamName]` `[FeedbackName]` `[Status]` - lets a person who is part of a team's board change the status of a feedback.

**Assign**
- AssignWorkItemToPerson `[PersonName]` `[BoardName]` `[TeamName]` `[WornItemName]` - assigns a specified work item to a specified person.
- UnassignWorkItemToPerson  `[PersonName]` `[BoardName]` `[TeamName]` `[WornItemName]` - unassigns a specified work item to a specified person.

**Filter**
- FilterBugs - shows all existing bugs.
- FilterStories - shows all existing stories.
- FilterFeedbacks - shows all existing feedbacks.
- FilterByAssignee `[AssigneeName]` - shows all work items with specific assignee.
- FilterByStatus `[Status]` - shows all work items with specific status

**Sort**
- SortWorkItemByPriority - shows all work items sorted by priority.
- SortWorkItemBySeverity - shows all work items sorted by severity.
- SortWorkItemBySize - shows all work items sorted by size.
- SortWorkItemByRating - shows all work items sorted by rating.

### ***Output screenshots***

![output1](/images/Output1.PNG)

![output2](/images/Output2.PNG)

![output3](/images/Output3.PNG)



