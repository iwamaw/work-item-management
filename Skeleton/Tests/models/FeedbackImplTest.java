package models;

import models.contracts.Feedback;
import org.junit.Test;


public class FeedbackImplTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_titleLessThanMinValue(){
        //Act
        Feedback feedback = new FeedbackImpl("dsa","sadsdas sdasd sadasdasdasd",3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_titleMoreThanMaxValue(){
        //Act
        Feedback feedback = new FeedbackImpl("dsadasdasddsadasdasddsadasdasddsadasdasddsadasdasddsadasdasd", "desascasjk sdasd sda",3);
    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_titleIsNull(){
        //Act
        Feedback feedback = new FeedbackImpl(null,"dasdasdsad sajkdhakshd asdha",3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_descriptionLessThanMinValue(){
        //Act
        Feedback feedback = new FeedbackImpl("ProblemSolved","lol",3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_descriptionMoreThanMaxValue(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= 502 ; i++) {
            String test = "text";
            stringBuilder.append(test);
        }
        //Act
        Feedback feedback = new FeedbackImpl("ProblemSolved",stringBuilder.toString(),3);
    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_descriptionIsNull(){
        //Act
        Feedback feedback = new FeedbackImpl("ProblemSolved",null,3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_when_ratingIsLessThanMin(){
        //Act
        Feedback feedback = new FeedbackImpl("ProblemSolved","sdadsa dasdasdasdasd sdad", -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_when_ratingIsMoreThanMax(){
        //Act
        Feedback feedback = new FeedbackImpl("ProblemSolved","sdadsa dasdasdasdasd sdad", 6);
    }



}