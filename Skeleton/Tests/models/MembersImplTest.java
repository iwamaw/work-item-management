package models;

import com.sun.corba.se.spi.orbutil.threadpool.Work;
import commands.list.AllStatusEnum;
import models.contracts.Bug;
import models.contracts.Members;
import models.contracts.WorkItems;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.List;

public class MembersImplTest {
    private WorkItems testWorkitem;
    private String testHistory;
    private Members testMember;

    @Before
    public void before(){
        testHistory = "Text";
        testMember = new MembersImpl("Ico");
        testWorkitem = new BugImpl("ProblemSolved","sdjajdjaskd asdasda");

    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_nameIsNull() {
        //Act
        Members member = new MembersImpl(null);
    }


    @Test
    public void check_get_workitems_array() {
        Members member = new MembersImpl("Ico");

        List<WorkItems> workingItemsList = new ArrayList<>(member.getListOfWorkingItems());
        workingItemsList.add(testWorkitem);
        Assert.assertEquals(1, workingItemsList.size());
    }

    @Test
    public void check_null_workitems_array() {
        Members member = new MembersImpl("Ico");
        Assert.assertEquals(0, member.getListOfWorkingItems().size());
    }

    @Test
    public void check_add_remove_workitem(){
        Members member = new MembersImpl("Ico");
        member.addWorkItem(testWorkitem);
        member.removeWorkItem(testWorkitem);
        Assert.assertEquals(0,member.getListOfWorkingItems().size());
    }

    @Test
    public void check_get_history_array() {
        List<String> historyList = new ArrayList<>(testMember.getHistory());
        historyList.add(testHistory);
        Assert.assertEquals(1, historyList.size());
    }

    @Test(expected = NullPointerException.class)
    public void check_null_history_array() {
        Members member = new MembersImpl("Ico");
        member.addToHistory(null);
        Assert.assertEquals(0, member.getHistory().size());
    }

    @Test
    public void check_add_history(){
        Members member = new MembersImpl("Iva");
        member.addToHistory(testHistory);
        Assert.assertEquals(1,member.getHistory().size());
    }

    @Test
    public void check_get_name(){
        Members member = new MembersImpl("Iva");
        Assert.assertEquals("Iva",member.getName());
    }



}