package models;

import models.contracts.Bug;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;

public class BugImplTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_titleLessThanMinValue() {
        Bug bug = new BugImpl("bug", "sadjsdkagsajdsadsad dsasdsa dasd");
        //Act
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_titleMoreThanMaxValue() {
        //Act
        Bug bug = new BugImpl("dsadasdasddsadasdasddsadasdasddsadasdasddsadasdasddsadasdasd", "desascasjk sdasd sda");
    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_titleIsNull() {
        //Act
        Bug bug = new BugImpl(null, "dasdasdsad sajkdhakshd asdha");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_descriptionLessThanMinValue() {
        //Act
        Bug bug = new BugImpl("ProblemSolved", "lol");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_descriptionMoreThanMaxValue() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= 502; i++) {
            String test = "text";
            stringBuilder.append(test);
        }
        //Act
        Bug bug = new BugImpl("ProblemSolved", stringBuilder.toString());
    }

    @Test(expected = NullPointerException.class)
    public void constructor_should_throw_when_descriptionIsNull() {
        //Act
        Bug bug = new BugImpl("ProblemSolved", null);
    }

    @Test
    public void check_add_array() {
        Bug bug = new BugImpl("ProblemSolved", "sdasdkasd sajdakshdsa dhsakjda");
        ArrayList<String> arr = new ArrayList<>();
        arr.add("LOL");
        Assert.assertEquals(1, arr.size());
    }

    @Test
    public void check_null_array() {
        Bug bug = new BugImpl("ProblemSolved", "sdasdkasd sajdakshdsa dhsakjda");
        Assert.assertEquals(0, bug.getStepToReproduce().size());
    }


}