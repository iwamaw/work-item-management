package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.*;
import models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class UnassignWorkItemToPersonTest {
    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private Boards testBoard;
    private Teams testTeam;
    private Bug testBug;
    private Story testStory;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new UnassignWorkItemToPerson(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testBoard=new BoardsImpl("BoardNameTest");
        testTeam=new TeamsImpl("TeamNameTest");
        testBug=new BugImpl("testBugName","testDescriptionBug");
        testStory= new StoryImpl("testStoryName","testDescriptishdjsaStory");
        itemRepository.addMembers(testPerson.getName(),testPerson);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
        itemRepository.addBug(testBug.getTitle(),testBug);
        itemRepository.addStory(testStory.getTitle(),testStory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        testList.add("cccc");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_AssignWorkItemToPerson_when_inputIsValid_forBug(){
        //Arrange
        testTeam.addMemberToTeam(testPerson);
        testTeam.addBoardToTeam(testBoard);
        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add(testBug.getTitle());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertNull(testBug.getAssignee());
    }

    @Test
    public void execute_should_AssignWorkItemToPerson_when_inputIsValid_forStory(){
        //Arrange
        testTeam.addMemberToTeam(testPerson);
        testTeam.addBoardToTeam(testBoard);
        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add(testStory.getTitle());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertNull(testStory.getAssignee());
    }

}