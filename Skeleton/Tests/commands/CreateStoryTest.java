package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import core.factories.WIMFactoryImpl;
import models.BoardsImpl;
import models.MembersImpl;
import models.StoryImpl;
import models.TeamsImpl;
import models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateStoryTest {
    private ItemRepository itemRepository;
    private WIMFactory wimFactory;
    private Command testCommand;
    private Members testMember;
    private Boards testBoard;
    private Teams testTeam;
    private Story testStory;


    @Before
    public void before(){
        wimFactory = new WIMFactoryImpl();
        itemRepository = new ItemRepositoryImpl();
        testCommand = new CreateStory(itemRepository,wimFactory);
        testMember = new MembersImpl("Ico");
        testBoard = new BoardsImpl("To Do List");
        testTeam = new TeamsImpl("NinjaRoni");
        testStory = new StoryImpl("Titlesdasdsad","Descriptiononono nononono no");
        itemRepository.addMembers(testMember.getName(),testMember);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
        //itemRepository.addStory(testStory.getTitle(),testStory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("LOLOLOL");
        testList.add("LOLOLOL");
        //Act
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        List<String> testList = new ArrayList<>();
        testList.add("LOLOLOL");
        testList.add("LOLOLOL");
        testList.add("LOLOLOL");
        testList.add("LOLOLOL");
        testList.add("LOLOLOL");
        testList.add("LOLOLOL");
        testList.add("LOLOLOL");

        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createStory_when_inputIsValid(){
        //Arrange
        testTeam.addBoardToTeam(testBoard);
        testTeam.addMemberToTeam(testMember);
        List<String> testList = new ArrayList<>();
        testList.add(testMember.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add("titletitletitle");
        testList.add("descriptiondescription");


        //Act
        testCommand.execute(testList);

        //Assert
        Assert.assertEquals(1,itemRepository.getStoryList().size());
    }

}