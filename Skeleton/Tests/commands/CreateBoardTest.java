package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import core.factories.WIMFactoryImpl;
import models.BoardsImpl;
import models.TeamsImpl;
import models.contracts.Boards;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateBoardTest {


    private Command testCommand;
    private ItemRepository itemRepository;
    private WIMFactory wimFactory;
    private Boards testBoard;
    private Teams testTeam;

    @Before
    public void before(){
        wimFactory=new WIMFactoryImpl();
        itemRepository=new ItemRepositoryImpl();
        testCommand=new CreateBoard(itemRepository,wimFactory);
        //testBoard=new BoardsImpl("BoardTestName");
        testTeam=new TeamsImpl("TeamTestName");
        itemRepository.addTeams(testTeam.getName(),testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void execute_should_throwExeption_when_teamDoesNotExist(){
//        //Arrange
//        List<String> testList=new ArrayList<>();
//        testList.add(testTeam.getName());
//        //Act&Assert
//        testCommand.execute(testList);
//    }

    @Test
    public void execute_should_createBoard_when_inputIsValid(){
        //Arrange
        itemRepository.addTeams(testTeam.getName(),testTeam);
        List<String> testList=new ArrayList<>();
        testList.add(testTeam.getName());
        testList.add("Board12");
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,itemRepository.getBoard().size());
    }

}