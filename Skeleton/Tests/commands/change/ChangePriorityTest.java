package commands.change;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BoardsImpl;
import models.BugImpl;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Boards;
import models.contracts.Bug;
import models.contracts.Members;
import models.contracts.Teams;
import models.enums.PriorityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ChangePriorityTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private Boards testBoard;
    private Teams testTeam;
    private Bug testItem;
    private PriorityType testType;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new ChangePriority(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testBoard=new BoardsImpl("boardNameTest");
        testTeam=new TeamsImpl("teamNameTest");
        testItem= new BugImpl("bugNameTest","DescriptionBugTest");
        testType=PriorityType.HIGH;
        itemRepository.addBug(testItem.getTitle(),testItem);
        itemRepository.addMembers(testPerson.getName(),testPerson);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("aaaa");
        testList.add("aaaa");

        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_ChangePriorityType_when_inputIsValid(){
        //Arrange
        testTeam.addBoardToTeam(testBoard);
        testTeam.addMemberToTeam(testPerson);
        testBoard.addWorkItem(testItem);

        PriorityType typeBefore=testItem.getPriorityType();
        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add(testItem.getTitle());
        testList.add(String.valueOf(testType));
        //Act
        testCommand.execute(testList);
       // PriorityType typeAfter=testItem.getPriorityType();
        //Asseert
        Assert.assertNotEquals(typeBefore,testType);
    }

}