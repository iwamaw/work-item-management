package commands.change;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BoardsImpl;
import models.BugImpl;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Boards;
import models.contracts.Bug;
import models.contracts.Members;
import models.contracts.Teams;
import models.enums.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ChangeSeverityOfBugTest {
    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private Boards testBoard;
    private Teams testTeam;
    private Bug testItem;
    private SeverityType testType;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new ChangeSeverityOfBug(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testBoard=new BoardsImpl("boardNameTest");
        testTeam=new TeamsImpl("teamNameTest");
        testItem=new BugImpl("BugNameTest","DescriptionTest");
        itemRepository.addBug(testItem.getTitle(),testItem);
        itemRepository.addMembers(testPerson.getName(),testPerson);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
        testType=SeverityType.CRITICAL;
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("aaaa");
        testList.add("aaaa");

        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_changeSeverityType_when_inputIsValid(){
        //Arrange
        SeverityType severityBefore=testItem.getSeverityType();
        testTeam.addBoardToTeam(testBoard);
        testTeam.addMemberToTeam(testPerson);
        testBoard.addWorkItem(testItem);

        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add(testItem.getTitle());
        testList.add(String.valueOf(testType));
        //Act
        testCommand.execute(testList);
        SeverityType severityAfter=testItem.getSeverityType();

        //Assert
        Assert.assertNotEquals(severityBefore,severityAfter);
    }

}