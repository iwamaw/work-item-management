package commands.change;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BoardsImpl;
import models.FeedbackImpl;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Boards;
import models.contracts.Feedback;
import models.contracts.Members;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ChangeRatingOfFeedbackTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private Boards testBoard;
    private Teams testTeam;
    private Feedback testItem;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testCommand=new ChangeRatingOfFeedback(itemRepository);
        testPerson=new MembersImpl("personNameTest");
        testBoard=new BoardsImpl("boardNameTest");
        testTeam=new TeamsImpl("teamNameTest");
        testItem=new FeedbackImpl("FeedbackNameTest","DescriptionTest",3);
        itemRepository.addFeedback(testItem.getTitle(),testItem);
        itemRepository.addMembers(testPerson.getName(),testPerson);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("aaaa");
        testList.add("aaaa");

        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }
    @Test
    public void execute_should_changeRatingOfFeedback_when_inputIsValid(){
        //Arrange
        testTeam.addMemberToTeam(testPerson);
        testTeam.addBoardToTeam(testBoard);
        testBoard.addWorkItem(testItem);

        int ratingBefore=testItem.getRating();
        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add(testItem.getTitle());
        testList.add(String.valueOf(2));
        //Act
        testCommand.execute(testList);
        int ratingAfter=testItem.getRating();
        //Assert
        Assert.assertNotEquals(ratingBefore,ratingAfter);
    }

}