package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Members;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowAllTeamsTest {
    private ItemRepository itemRepository;
    private Teams testTeam;
    private Command testCommand;
    private List<String> testShow;


    @Before
    public void before(){
        itemRepository = new ItemRepositoryImpl();
        testTeam = new TeamsImpl("NinjaRoni");
        testCommand = new ShowAllTeams(itemRepository);
        testShow = new ArrayList<>();
        testShow.add(testTeam.getName());

    }

    @Test
    public void execute_should_ShowAllTeams_when_inputIsValid(){
        //Arrange
        itemRepository.addTeams(testTeam.getName(),testTeam);
        List<String> testList = new ArrayList<>();
        List<String> testShowAfter = new ArrayList<>(itemRepository.getTeams().keySet());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testShow.toString(),testShowAfter.toString());

    }
}