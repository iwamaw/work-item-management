package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import core.factories.WIMFactoryImpl;
import models.BoardsImpl;
import models.BugImpl;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Boards;
import models.contracts.Bug;
import models.contracts.Members;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateBugTest {

    private ItemRepository itemRepository;
    private WIMFactory wimFactory;
    private Members testPerson;
    private Boards testBoard;
    private Teams testTeam;
    private Command testCommand;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        wimFactory=new WIMFactoryImpl();
        testPerson=new MembersImpl("personNameTest");
        testBoard=new BoardsImpl("BoardNameTest");
        testTeam=new TeamsImpl("TeamNameTest");
        //testBug=new BugImpl("NameTestBug","TestDescriptionBug");
        testCommand=new CreateBug(itemRepository,wimFactory);
        itemRepository.addMembers(testPerson.getName(),testPerson);
        itemRepository.addBoards(testBoard.getName(),testBoard);
        itemRepository.addTeams(testTeam.getName(),testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("cccc");
        testList.add("cccc");
        testList.add("cccc");
        testList.add("cccc");
        testList.add("cccc");
        //Act&Assert
        testCommand.execute(testList);
    }


    @Test
    public void execute_should_createBug_when_inputIsValid(){

        testTeam.addBoardToTeam(testBoard);
        testTeam.addMemberToTeam(testPerson);
        List<String> testList=new ArrayList<>();
        testList.add(testPerson.getName());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testList.add("TitleOfBug");
        testList.add("DescriptionOgBug");
        testList.add("StepsToReproduce");
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,itemRepository.getBugList().size());
    }


}