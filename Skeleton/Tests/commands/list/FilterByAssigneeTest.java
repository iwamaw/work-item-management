package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BugImpl;
import models.MembersImpl;
import models.StoryImpl;
import models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class FilterByAssigneeTest {

    private ItemRepository itemRepository;
    private Command testCommand;
    private Members testPerson;
    private Members testPerson1;
    private Bug testBug;
    private Story testStory;
    private List<BugAndStory> testShow;

    @Before
    public void before() {
        itemRepository = new ItemRepositoryImpl();
        testCommand = new FilterByAssignee(itemRepository);
        testPerson = new MembersImpl("personNameTest");
        testPerson1 = new MembersImpl("personNameTest1");
        testBug = new BugImpl("bugNameTest", "descriptionTest");
        testStory = new StoryImpl("storyNameTest", "descriptionTest");
        itemRepository.addMembers(testPerson.getName(), testPerson);
        itemRepository.addMembers(testPerson1.getName(), testPerson1);
        testBug.setAssignee(testPerson);
        testStory.setAssignee(testPerson1);
        testShow = new ArrayList<>();
        testShow.add(testBug);
        testShow.add(testStory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_FilterByAssignee() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testPerson.getName());

        List<WorkItems> testShowAfter = new ArrayList<>(itemRepository.getItems().values());

        for (BugAndStory item : testShow) {
            if (item.getAssignee().equals(testPerson))
                testShowAfter.add(item);
        }
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertNotEquals(testShow, testShowAfter);
    }

}