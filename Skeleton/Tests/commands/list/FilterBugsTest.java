package commands.list;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BugImpl;
import models.contracts.Bug;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterBugsTest {
    private ItemRepository itemRepository;
    private Bug testBug;
    private List<Bug> testBugList;
    private Command testCommand;

    @Before
    public void before() {
        itemRepository = new ItemRepositoryImpl();
        testBug = new BugImpl("Bugtitletitle", "Bugdescriptiondescription");
        itemRepository.addBug(testBug.getTitle(), testBug);
        testCommand = new FilterBugs(itemRepository);
        testBugList = new ArrayList<>();
        testBugList.add(testBug);
    }

    @Test
    public void execute_should_FilterBugs() {
        //Arrange
        List<String> testList = new ArrayList<>();
        List<Bug> testBugListAfter = new ArrayList<Bug>(itemRepository.getBugList().values());
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testBugList.toString(), testBugListAfter.toString());
    }
}