package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.BoardsImpl;
import models.TeamsImpl;
import models.contracts.Boards;
import models.contracts.Members;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowTeamAllBoardsTest {
    private ItemRepository itemRepository;
    private Teams testTeam;
    private Boards testBoard;
    private List<Boards> testShow;
    private Command testCommand;

    @Before
    public void before(){
        itemRepository = new ItemRepositoryImpl();
        testTeam = new TeamsImpl("NINJARONI");
        testBoard = new BoardsImpl("NINJARONISDSA");
        testTeam.addBoardToTeam(testBoard);
        testShow = testTeam.getBoards();
        testCommand = new ShowTeamAllBoards(itemRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("aaaa");
        testList.add("bbbb");
        testList.add("aaaa");
        testList.add("bbbb");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_showAllTeamBoards(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        itemRepository.addTeams(testTeam.getName(),testTeam);
        List<Boards> showTestAfter = itemRepository.getTeams().get(testTeam.getName()).getBoards();
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testShow.toString(),showTestAfter.toString());
    }

}