package commands;

import commands.contracts.Command;
import core.ItemRepositoryImpl;
import core.contracts.ItemRepository;
import models.MembersImpl;
import models.TeamsImpl;
import models.contracts.Members;
import models.contracts.Teams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowTeamAllMembersTest {

    private ItemRepository itemRepository;
    private Teams testTeam;
    private Members testMember;
    private List<Members> testShow;
    private Command testCommand;

    @Before
    public void before(){
        itemRepository=new ItemRepositoryImpl();
        testTeam=new TeamsImpl("teamNameTest");
        testMember=new MembersImpl("memberNameTest");
        testTeam.addMemberToTeam(testMember);
        testShow=testTeam.getMembers();
        testCommand=new ShowTeamAllMembers(itemRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add("aaaa");
        testList.add("bbbb");
        //Act&Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_showAllTeamMember(){
        //Arrange
        List<String> testList=new ArrayList<>();
        testList.add(testTeam.getName());

        itemRepository.addTeams(testTeam.getName(),testTeam);
        List<Members> showTestAfter =itemRepository.getTeams().get(testTeam.getName()).getMembers();
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(testShow.toString(),showTestAfter.toString());
    }


}