package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Members;
import models.contracts.Teams;

import java.util.List;

import static commands.CommandConstants.*;


public class AddPersonToTeam implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ItemRepository itemRepository;

    public AddPersonToTeam(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        String teamToAddToName = parameters.get(0);
        String personToBeAddedName = parameters.get(1);
        return addPersonToTeam(teamToAddToName, personToBeAddedName);
    }

    private String addPersonToTeam(String teamToAddToName, String personToBeAddedName) {

        if (!itemRepository.getTeams().containsKey(teamToAddToName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamToAddToName));
        }

        if (!itemRepository.getMembers().containsKey(personToBeAddedName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personToBeAddedName));
        }

        Teams team = itemRepository.getTeams().get(teamToAddToName);
        Members person = itemRepository.getMembers().get(personToBeAddedName);

        team.addMemberToTeam(person);
        person.addToHistory(String.format(ADD_PERSON_TO_TEAM_HISTORY_MESSAGE, personToBeAddedName, teamToAddToName));

        return String.format(PERSON_ADDED_SUCCESS_MESSAGE, personToBeAddedName, teamToAddToName);
    }

}
