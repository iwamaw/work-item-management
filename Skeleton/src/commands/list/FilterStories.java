package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Story;

import java.util.ArrayList;
import java.util.List;

import static commands.list.CommandListConstants.*;

public class FilterStories implements Command {

    private final ItemRepository itemRepository;

    public FilterStories(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (itemRepository.getStoryList().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_STORIES);
        }

        List<Story> storyList = new ArrayList<>(itemRepository.getStoryList().values());

        StringBuilder stringBuilder = new StringBuilder();
        for (Story story : storyList) {
            stringBuilder.append(story.toString());
        }
        return stringBuilder.toString();
    }
}
