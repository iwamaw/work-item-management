package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Feedback;

import java.util.ArrayList;
import java.util.List;

import static commands.list.CommandListConstants.*;

public class FilterFeedback implements Command {

    private final ItemRepository itemRepository;

    public FilterFeedback(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (itemRepository.getFeedbackList().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_FEEDBACK);
        }

        List<Feedback> feedbackList = new ArrayList<>(itemRepository.getFeedbackList().values());

        StringBuilder stringBuilder = new StringBuilder();
        for (Feedback feedback : feedbackList) {
            stringBuilder.append(feedback.toString());
        }
        return stringBuilder.toString();
    }
}
