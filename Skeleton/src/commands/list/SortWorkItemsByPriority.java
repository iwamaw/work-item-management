package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.BugAndStory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static commands.list.CommandListConstants.*;

public class SortWorkItemsByPriority implements Command {

    private ItemRepository itemRepository;

    public SortWorkItemsByPriority(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (itemRepository.getStoryList().size() == 0 && itemRepository.getBugList().size() == 0) {
            throw new IllegalArgumentException(EMPTY_LIST_ITEMS);
        }

        List<BugAndStory> listOfValue = new ArrayList<>();
        listOfValue.addAll(itemRepository.getStoryList().values());
        listOfValue.addAll(itemRepository.getBugList().values());

        List<BugAndStory> sortedList = listOfValue.stream()
                .sorted(Comparator.comparing(BugAndStory::getPriorityType))
                .collect(Collectors.toList());


        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("LIST OF BUG AND STORY SORTED BY PRIORITY: %n"));
        for (BugAndStory temp : sortedList) {
            stringBuilder.append(temp.toString());
        }
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();

    }
}
