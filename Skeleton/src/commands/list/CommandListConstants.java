package commands.list;

public class CommandListConstants {

    //Error messages
    static final String NO_EXISTING_BUG = "There is no existing bug";
    static final String NO_EXISTING_FEEDBACK = "There is no existing feedback";
    static final String NO_EXISTING_STORIES = "There is no existing stories";
    static final String NO_EXISTING_ITEM = "There is no existing work item";
    static final String PERSON_NOT_EXISTS_ERROR_MESSAGE = "Person with name: %s do not exist";

    //Empty List messages
    static final String EMPTY_LIST_RATING = "Empty list of feedback";
    static final String EMPTY_LIST_ITEMS = "Empty list of items";
    static final String EMPTY_LIST_BUGS = "Empty list of bugs";
    static final String EMPTY_LIST_STORIES = "Empty list of stories";
    static final String EMPTY_LIST_WORK_ITEMS = "Empty list of work items";

}
