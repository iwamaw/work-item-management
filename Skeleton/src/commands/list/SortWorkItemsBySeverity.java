package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Bug;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static commands.list.CommandListConstants.*;

public class SortWorkItemsBySeverity implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private ItemRepository itemRepository;

    public SortWorkItemsBySeverity(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }


    public String execute(List<String> parameters) {

        if (itemRepository.getBugList().size() == 0) {
            throw new IllegalArgumentException(EMPTY_LIST_BUGS);
        }

        List<Bug> listOfBugValues = new ArrayList<>(itemRepository.getBugList().values());

        List<Bug> sortedList = listOfBugValues.stream()
                .sorted(Comparator.comparing(Bug::getSeverityType))
                .collect(Collectors.toList());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("LIST OF BUGS SORTED BY SEVERITY: %n"));
        for (Bug bug : sortedList) {
            stringBuilder.append(bug.toString());
        }
        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();


    }


}
