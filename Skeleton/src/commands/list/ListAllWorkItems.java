package commands.list;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;

import static commands.list.CommandListConstants.*;

public class ListAllWorkItems implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ItemRepository itemRepository;


    public ListAllWorkItems(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (itemRepository.getItems().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_ITEM);
        }

        List<WorkItems> listOfWorkItems = new ArrayList<>(itemRepository.getItems().values());

        StringBuilder stringBuilder = new StringBuilder();
        for (WorkItems workItems : listOfWorkItems) {
            stringBuilder.append(workItems.toString());
        }

        stringBuilder.append(System.lineSeparator());
        return stringBuilder.toString();
    }
}
