package commands.list;

public enum AllStatusEnum {
    ACTIVE,
    FIXED,
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE,
    NOTDONE,
    INPROGRESS;

    public String toString(){
        switch (this){
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            case NOTDONE:
                return "Not done";
            case INPROGRESS:
                return "In progress";
            default:
                throw new IllegalArgumentException();
        }
    }

}
