package commands.change;

import java.util.Locale;

public class ChangeCommandConstant {

    //error messages
    static final String EMPTY_LIST_OF_STORY="Empty list of stories";
    static final String EMPTY_LIST_OF_BUGS="Empty list of bugs";
    static final String EMPTY_LIST_OF_FEEDBACK="Empty list of feedback";
    static final String EMPTY_LIST_OF_ITEMS="Empty list of items";
    static final String NO_EXISTING_WORK_ITEM = "Work item with name: %s do not exist";
    static final String PERSON_NOT_EXISTS_ERROR_MESSAGE = "Person with name: %s do not exist";
    static final String BOARD_NOT_EXISTS_ERROR_MESSAGE = "Board with name: %s do not exist";
    static final String TEAM_NOT_EXISTS_ERROR_MESSAGE = "Team with name: %s does not exist";
    static final String NOT_EXISTING_ITEM_ERROR_MESSAGE="Work item with name: %s do not exist";
    static final String NO_EXISTING_PERSON_OR_BOARD_IN_TEAM=
            "No existing person with name: %s or board with name: %s in team with name: %s";


    //success messages
     static final String SUCCESSFULLY_CHANGED_STORY_SIZE = "The size of story with name: %s was changed";
     static final String SUCCESSFULLY_CHANGED_SEVERITY_OF_BUG = "The severity of bug with name: %s was changed";
     static final String SUCCESSFULLY_CHANGED_RATING_OF_FEEDBACK = "The rating of feedback with name: %s was changed";
     static final String SUCCESSFULLY_CHANGED_PRIORITY = "Priority of work item with name: %s was changed";
     static final String ADD_TO_BOARD_HISTORY="Member with name: %s changed priority of item with name: %s";
     static final String ADD_TO_PERSON_HISTORY="You changed priority of item with name: %s";
     static final String ADD_TO_BOARD_HISTORY_CHANGERATING="Member with name: %s changed rating of feedback: %s";
     static final String ADD_TO_PERSON_HISTORY_CHANGEPRIORITY="You changed rating of feedback: %s";
     static final String ADD_TO_PERSON_HISTORY_CHANGESEVERITY="You changed severity of bug: %s";
     static final String ADD_TO_BOARD_HISTORY_CHANGESEVERITY="Member with name: %s changed severity of bug: %s";
     static final String ADD_TO_BOARD_HISTORY_CHANGESIZE="Member with name: %s changed size of story: %s";
     static final String ADD_TO_PERSON_HISTORY_CHANGESIZE="You changed size of story: %s";
     static final String ADD_TO_PERSON_HISTORY_CHANGESTATUS="You changed status of work item: %s";
     static final String ADD_TO_BOARD_HISTORY_CHANGESTATUS="Member with name: %s changed status of item: %s";
    static String SUCCESSFULLY_CHANGED_STATUS_OF_BUG="Status of bug with name: %s was changed";
     static String SUCCESSFULLY_CHANGED_STATUS_OF_STORY="Status of story with name: %s was changed";
     static String SUCCESSFULLY_CHANGED_STATUS_OF_FEEDBACK="Status of feedback with name: %s was change";
}
