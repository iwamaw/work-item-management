package commands.change;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Boards;
import models.contracts.Members;
import models.contracts.Story;
import models.contracts.Teams;
import models.enums.SizeType;
import java.util.List;

import static commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static commands.change.ChangeCommandConstant.*;

public class ChangeSizeOfStory implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private ItemRepository itemRepository;

    public ChangeSizeOfStory(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        if (itemRepository.getStoryList().size() == 0) {
            throw new IllegalArgumentException(EMPTY_LIST_OF_STORY);
        }
        String personName=parameters.get(0);
        String boardName=parameters.get(1);
        String teamName=parameters.get(2);
        String storyName = parameters.get(3);
        String size = parameters.get(4);

        return changeSizeOfStory(personName,boardName,teamName,storyName, size);
    }

    private String changeSizeOfStory(String personName,String boardName,String teamName,String storyName, String size) {

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if(!itemRepository.getTeams().containsKey(teamName)){
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE,teamName));
        }
        if(!itemRepository.getStoryList().containsKey(storyName)){
            throw new IllegalArgumentException(String.format(NO_EXISTING_WORK_ITEM,storyName));
        }

        Members person=itemRepository.getMembers().get(personName);
        Boards board=itemRepository.getBoard().get(boardName);
        Teams team=itemRepository.getTeams().get(teamName);
        Story story = itemRepository.getStoryList().get(storyName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        if(!board.getListOfWorkingItems().contains(story)){
            return String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE,storyName);
        }

        SizeType sizeType = SizeType.valueOf(size.toUpperCase());
        story.setSize(sizeType);
        board.addToHistory(String.format(ADD_TO_BOARD_HISTORY_CHANGESIZE,personName,storyName));
        person.addToHistory(String.format(ADD_TO_PERSON_HISTORY_CHANGESIZE,storyName));

        return String.format(SUCCESSFULLY_CHANGED_STORY_SIZE, storyName);
    }


}
