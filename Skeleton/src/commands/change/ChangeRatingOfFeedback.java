package commands.change;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.*;
import org.omg.PortableInterceptor.SUCCESSFUL;

import java.util.List;

import static commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static commands.change.ChangeCommandConstant.*;

public class ChangeRatingOfFeedback implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;


    private ItemRepository itemRepository;

    public ChangeRatingOfFeedback(ItemRepository itemRepository){
        this.itemRepository=itemRepository;
    }

    public String execute(List<String> parameters){

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        if(itemRepository.getFeedbackList().size()==0){
            throw new IllegalArgumentException(EMPTY_LIST_OF_FEEDBACK);
        }

        String personName=parameters.get(0);
        String boardName=parameters.get(1);
        String teamName=parameters.get(2);
        String feedbackName=parameters.get(3);
        int rating=Integer.parseInt(parameters.get(4));
        return changeRatingOfFeedback(personName,boardName,teamName,feedbackName,rating);
    }

    private String changeRatingOfFeedback(String personName,String boardName,String teamName,String feedbackName, int rating){

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if(!itemRepository.getTeams().containsKey(teamName)){
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE,teamName));
        }
        if(!itemRepository.getFeedbackList().containsKey(feedbackName)){
            throw new IllegalArgumentException(String.format(NO_EXISTING_WORK_ITEM,feedbackName));
        }

        Members person=itemRepository.getMembers().get(personName);
        Boards board=itemRepository.getBoard().get(boardName);
        Teams team=itemRepository.getTeams().get(teamName);
        Feedback feedback=itemRepository.getFeedbackList().get(feedbackName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        if(!board.getListOfWorkingItems().contains(feedback)){
            return String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE,feedbackName);
        }
        feedback.setRating(rating);
        board.addToHistory(String.format(ADD_TO_BOARD_HISTORY_CHANGERATING,personName,feedbackName));
        person.addToHistory(String.format(ADD_TO_PERSON_HISTORY_CHANGEPRIORITY,feedbackName));
        return String.format(SUCCESSFULLY_CHANGED_RATING_OF_FEEDBACK,feedbackName);
    }

}
