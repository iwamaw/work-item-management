package commands.change;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Boards;
import models.contracts.Members;
import models.contracts.Story;
import models.contracts.Teams;
import models.enums.StoryStatusType;

import static commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static commands.change.ChangeCommandConstant.*;
import java.util.List;

public class ChangeStatusOfStory implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private ItemRepository itemRepository;

    public ChangeStatusOfStory(ItemRepository itemRepository){
        this.itemRepository=itemRepository;
    }

    public String execute(List<String> parameters){
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        if(itemRepository.getStoryList().size()==0){
            throw new IllegalArgumentException(EMPTY_LIST_OF_STORY);
        }

        String personName=parameters.get(0);
        String boardName=parameters.get(1);
        String teamName=parameters.get(2);
        String storyName=parameters.get(3);
        String status=parameters.get(4);

        return changeStatusOfStory(personName,boardName,teamName,storyName,status);
    }

    private String changeStatusOfStory(String personName,String boardName,String teamName,String storyName, String status){

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if(!itemRepository.getTeams().containsKey(teamName)){
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE,teamName));
        }
        if(!itemRepository.getStoryList().containsKey(storyName)){
            throw new IllegalArgumentException(String.format(NO_EXISTING_WORK_ITEM,storyName));
        }

        Members person=itemRepository.getMembers().get(personName);
        Boards board=itemRepository.getBoard().get(boardName);
        Teams team=itemRepository.getTeams().get(teamName);
        Story story=itemRepository.getStoryList().get(storyName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        if(!board.getListOfWorkingItems().contains(story)){
            return String.format(NOT_EXISTING_ITEM_ERROR_MESSAGE,storyName);
        }
        StoryStatusType statusType=StoryStatusType.valueOf(status.toUpperCase().replaceAll("\\s",""));
        story.setStatus(statusType);
        person.addToHistory(String.format(ADD_TO_PERSON_HISTORY_CHANGESTATUS,storyName));
        board.addToHistory(String.format(ADD_TO_BOARD_HISTORY_CHANGESTATUS,personName,storyName));
        return String.format(SUCCESSFULLY_CHANGED_STATUS_OF_STORY,storyName);
    }

}
