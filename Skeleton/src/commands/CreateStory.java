package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import models.contracts.*;

import java.util.List;

import static commands.CommandConstants.*;

public class CreateStory implements Command {


    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private final WIMFactory wimFactory;
    private final ItemRepository itemRepository;

    public CreateStory(ItemRepository itemRepository, WIMFactory wimFactory) {
        this.wimFactory = wimFactory;
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        String personName = parameters.get(0);
        String boardName = parameters.get(1);
        String teamName = parameters.get(2);
        String title = parameters.get(3);
        String description = parameters.get(4);
        return createStory(personName, boardName, teamName, title, description);
    }

    private String createStory(String personName, String boardName, String teamName, String title, String description) {

        if (!itemRepository.getMembers().containsKey(personName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_EXISTS_ERROR_MESSAGE, personName));
        }
        if (!itemRepository.getBoard().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, boardName));
        }
        if (!itemRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamName));
        }

        Members person = itemRepository.getMembers().get(personName);
        Boards board = itemRepository.getBoard().get(boardName);
        Teams team = itemRepository.getTeams().get(teamName);

        if (!(team.getMembers().contains(person) && team.getBoards().contains(board))) {
            return String.format(NO_EXISTING_PERSON_OR_BOARD_IN_TEAM, personName, boardName, teamName);
        }

        Story story = wimFactory.createStory(title, description);
        itemRepository.addItems(title, story);
        itemRepository.addStory(title, story);

        board.addWorkItem(story);
        person.addToHistory(String.format(ADD_STORY_TO_HISTORY_MESSAGE, personName, boardName));
        board.addToHistory(String.format(ADD_STORY_TO_HISTORY_MESSAGE, personName, boardName));

        return String.format(STORY_CREATED_SUCCESS_MESSAGE, title);
    }

}
