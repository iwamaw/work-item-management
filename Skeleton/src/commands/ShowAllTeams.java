package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;

import java.util.ArrayList;
import java.util.List;

import static commands.CommandConstants.*;

public class ShowAllTeams implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowAllTeams(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    private final ItemRepository itemRepository;

    @Override
    public String execute(List<String> parameters) {
        if (itemRepository.getTeams().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_TEAM);
        }
        List<String> allTeams = new ArrayList<>(itemRepository.getTeams().keySet());
        return allTeams.toString();
    }

}
