package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;

import java.util.ArrayList;
import java.util.List;

import static commands.CommandConstants.*;

public class ShowAllPeople implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ItemRepository itemRepository;

    public ShowAllPeople(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public String execute(List<String> parameters) {

        if (itemRepository.getMembers().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_PEOPLE);
        }

        List<String> allPeople = new ArrayList<>(itemRepository.getMembers().keySet());
        return allPeople.toString();
    }

}
