package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import models.contracts.Members;

import java.util.List;

import static commands.CommandConstants.*;

public class CreateMember implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ItemRepository itemRepository;
    private final WIMFactory wimFactory;

    public CreateMember(WIMFactory wimFactory, ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        String memberName = parameters.get(0);
        return createMember(memberName);
    }

    private String createMember(String name) {

        if (itemRepository.getMembers().containsKey(name)) {
            return String.format(MEMBER_EXISTS_ERROR_MESSAGE, name);
        }

        Members member = wimFactory.createMember(name);
        itemRepository.addMembers(name, member);
        member.addToHistory(String.format(ADD_PERSON_TO_HISTORY_MESSAGE, name));

        return String.format(MEMBER_CREATED_SUCCESS_MESSAGE, name);
    }
}
