package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Boards;

import java.util.List;

import static commands.CommandConstants.*;

public class ShowBoardActivity implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ItemRepository itemRepository;

    public ShowBoardActivity(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        if (itemRepository.getBoard().size() == 0) {
            throw new IllegalArgumentException(NO_EXISTING_BOARDS);
        }
        String BoardHistory = parameters.get(0);
        return showBoardHistory(BoardHistory);
    }

    private String showBoardHistory(String BoardHistory) {

        if (!itemRepository.getBoard().containsKey(BoardHistory)) {
            throw new IllegalArgumentException(String.format(BOARD_NOT_EXISTS_ERROR_MESSAGE, BoardHistory));
        }
        Boards board = itemRepository.getBoard().get(BoardHistory);
        return board.historyToString();
    }
}
