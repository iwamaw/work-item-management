package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import models.contracts.Members;
import models.contracts.Teams;

import java.util.List;

import static commands.CommandConstants.*;

public class ShowTeamAllMembers implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowTeamAllMembers(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    private final ItemRepository itemRepository;

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        String showAllTeamMembers = parameters.get(0);
        return showAllTeamMembers(showAllTeamMembers);
    }

    private String showAllTeamMembers(String teamName) {

        if (!itemRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_EXISTS_ERROR_MESSAGE, teamName));
        }

        Teams team = itemRepository.getTeams().get(teamName);
        List<Members> members = team.getMembers();

        if (members.size() == 0) {
            return String.format(NO_MEMBERS_IN_TEAM, teamName);
        }
        return members.toString();
    }
}
