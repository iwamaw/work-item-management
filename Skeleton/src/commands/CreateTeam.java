package commands;

import commands.contracts.Command;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;
import models.contracts.Teams;

import java.util.List;

import static commands.CommandConstants.*;

public class CreateTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ItemRepository itemRepository;
    private final WIMFactory wimFactory;

    public CreateTeam(ItemRepository itemRepository, WIMFactory wimFactory) {
        this.itemRepository = itemRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        String teamName = parameters.get(0);
        return createTeam(teamName);
    }

    private String createTeam(String name) {

        if (itemRepository.getTeams().containsKey(name)) {
            return String.format(TEAM_EXISTS_ERROR_MESSAGE, name);
        }

        Teams team = wimFactory.createTeam(name);
        itemRepository.addTeams(name, team);

        return String.format(TEAM_CREATED_SUCCESS_MESSAGE, name);
    }
}
