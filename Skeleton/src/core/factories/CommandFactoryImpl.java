package core.factories;

import commands.*;
import commands.change.*;
import commands.contracts.Command;
import commands.enums.CommandType;
import commands.ShowAllTeams;
import commands.ShowTeamAllBoards;
import commands.ShowTeamAllMembers;
import commands.list.*;
import core.contracts.CommandFactory;
import core.contracts.ItemRepository;
import core.contracts.WIMFactory;

public class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Invalid command name: %s";

    public Command createCommand(String commandTypeAsString, WIMFactory wimFactory, ItemRepository itemRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType) {
            case CREATEMEMBER:
                return new CreateMember(wimFactory, itemRepository);
            case CREATEBOARD:
                return new CreateBoard(itemRepository, wimFactory);
            case CREATETEAM:
                return new CreateTeam(itemRepository, wimFactory);
            case CREATEBUG:
                return new CreateBug(itemRepository, wimFactory);
            case CREATESTORY:
                return new CreateStory(itemRepository, wimFactory);
            case CREATEFEEDBACK:
                return new CreateFeedback(itemRepository, wimFactory);
            case SHOWALLTEAMS:
                return new ShowAllTeams(itemRepository);
            case SHOWALLTEAMBOARDS:
                return new ShowTeamAllBoards(itemRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowTeamAllMembers(itemRepository);
            case SHOWALLPEOPLE:
                return new ShowAllPeople(itemRepository);
            case SHOWPEOPLEACTIVITY:
                return new ShowPeopleActivity(itemRepository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivity(itemRepository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivity(itemRepository);
            case SHOWWORKITEMSBYTITLE:
                return new SortWorkItemsByTitle(itemRepository);
            case SHOWFILTERBUGS:
                return new FilterBugs(itemRepository);
            case SHOWFILTERFEEDBACK:
                return new FilterFeedback(itemRepository);
            case SHOWFILTERSTORIES:
                return new FilterStories(itemRepository);
            case SHOWWORKITEMSBYPRIORITY:
                return new SortWorkItemsByPriority(itemRepository);
            case SHOWWORKITEMSBYSEVERITY:
                return new SortWorkItemsBySeverity(itemRepository);
            case SHOWWORKITEMSBYSIZE:
                return new SortWorkItemsBySize(itemRepository);
            case SHOWWORKITEMSBYRATING:
                return new SortWorkItemByRating(itemRepository);
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(itemRepository);
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentToWorkItem(itemRepository);
            case SHOWALLWORKITEMS:
                return new ListAllWorkItems(itemRepository);

            case CHANGESIZEOFSTORY:
                return new ChangeSizeOfStory(itemRepository);
            case CHANGERATINGOFFEEDBACK:
                return new ChangeRatingOfFeedback(itemRepository);
            case CHANGESEVERITYOFBUG:
                return new ChangeSeverityOfBug(itemRepository);
            case CHANGEPRIORITY:
                return new ChangePriority(itemRepository);
            case CHANGESTATUSOFSTORY:
                return new ChangeStatusOfStory(itemRepository);
            case CHANGESTATUSOFFEEDBACK:
                return new ChangeStatusOfFeedback(itemRepository);
            case CHANGESTATUSOFBUG:
                return new ChangeStatusOfBug(itemRepository);
            case FILTERBYSTATUS:
                return new FilterByStatus(itemRepository);
            case FILTERBYASSIGNEE:
                return new FilterByAssignee(itemRepository);
            case ASSIGNWORKITEMTOPERSON:
                return new AssignWorkItemToPerson(itemRepository);
            case UNASSIGNWORKITEMTOPERSON:
                return new UnassignWorkItemToPerson(itemRepository);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }

}
