package core.factories;

import core.contracts.WIMFactory;
import models.*;
import models.contracts.*;

public class WIMFactoryImpl implements WIMFactory {

    public Members createMember(String name) {
        return new MembersImpl(name);
    }

    public Boards createBoard(String name) {
        return new BoardsImpl(name);
    }

    public Teams createTeam(String name) {
        return new TeamsImpl(name);
    }

    public Bug createBug(String title, String description) {
        return new BugImpl(title, description);
    }

    public Story createStory(String title, String description) {
        return new StoryImpl(title, description);
    }

    public Feedback createFeedback(String title, String description, int rating) {
        return new FeedbackImpl(title, description, rating);
    }

}
