package core.contracts;

import models.contracts.*;

public interface WIMFactory {

    Members createMember(String name);
    Boards createBoard(String name);
    Teams createTeam(String name);
    Bug createBug(String title, String description);
    Story createStory(String title, String description);
    Feedback createFeedback(String title, String description,int rating);

}
