package models;

import models.contracts.Members;
import models.contracts.WorkItems;
import java.util.ArrayList;
import java.util.List;


public class MembersImpl implements Members {

    private String name;
    private List<WorkItems> workItems;
    private List<String> history;

    public MembersImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        history = new ArrayList<>();
    }

    public List<WorkItems> getListOfWorkingItems() {
        return new ArrayList<>(workItems);
    }

    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    public String getName() {
        return name;
    }

    public void addWorkItem(WorkItems workItem) {
        workItems.add(workItem);
    }

    public void removeWorkItem(WorkItems workItem) {
        workItems.remove(workItem);
    }

    public void addToHistory(String activity) {
        ValidationHelper.checkNull(activity);
        history.add(activity);
    }

    @Override
    public String toString() {
        return String.format("Member Name: %s", getName());
    }

    public String printHistory() {
        if (history.size() == 0) {
            return String.format("#History: %s%n" +
                    " #No activity in this history", name);
        }
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("#History of: %s%n", name));
        for (String activity : history) {
            strBuilder.append(activity);
            strBuilder.append(System.getProperty("line.separator"));
        }
        return strBuilder.toString();
    }

    private void setName(String name) {
        ValidationHelper.checkFunctionalName(name);
        this.name = name;
    }

}
