package models;

import models.contracts.Comment;
import models.contracts.Members;

public class CommentImpl implements Comment {

    private Members person;
    private String message;

    public CommentImpl(Members person, String message){
        setName(person);
        setMessage(message);
    }

    private void setName(Members person){
        ValidationHelper.checkNull(person);
        this.person=person;
    }

    private void setMessage(String message){
        ValidationHelper.checkNull(message);
        this.message=message;
    }

    public Members getName(){
        return person;
    }

    public String getMessage(){
        return message;
    }

    public String toString(){
        return String.format("Message from %s : %s",getName(),getMessage());
    }

}
