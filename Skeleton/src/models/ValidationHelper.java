package models;

import models.contracts.Boards;
import models.contracts.Members;
import models.contracts.WorkItems;

public class ValidationHelper {
    private static final int MIN_TITLE_VALUE = 10;
    private static final int MAX_TITLE_VALUE = 50;
    private static final String TITLE_INPUT_MESSAGE = String.format("Title must be between %d and %d symbols",MIN_TITLE_VALUE,MAX_TITLE_VALUE);
    private static final int MIN_DESCRIPTION_VALUE = 10;
    private static final int MAX_DESCRIPTION_VALUE = 500;
    private static final String DESCRIPTION_INPUT_MESSAGE = String.format("Description must be between %d and %d symbols",MIN_DESCRIPTION_VALUE,MAX_DESCRIPTION_VALUE);
    private static final String NULL_POINTER_MEMBER="NULL VALUE OF MEMBER";
    private static final String NULL_POINTER_TITLE="NULL VALUE OF TITLE";
    private static final String NULL_POINTER_DESCRIPTION="NULL VALUE OF DESCRIPTION";
    private static final String NULL_POINTER_FUNCTIONAL_NAME="NULL VALUE OF FUNCTIONAL NAME";
    private static final String NULL_POINTER_BOARD = "NULL VALUE OF BOARD";
    private static final String NULL_POINTER_WORKITEM ="NULL VALUE OF WORKITEM";
    private static final String NULL_POINTER_ACTIVITY="NULL VALUE OF ACTIVITY";
    private static final int MIN_RATING_VALUE = 0;
    private static final int MAX_RATING_VALUE = 5;
    private static final String RATING_INPUT_MESSAGE = String.format("Rating should be between %d and %d",MIN_RATING_VALUE,MAX_RATING_VALUE);


    public static void checkNull(String toCheck,String message){
        if(toCheck==null){
            throw new NullPointerException(message);
        }
    }
    public static void checkNull(String toCheck){
        if(toCheck==null){
            throw new NullPointerException(NULL_POINTER_ACTIVITY);
        }
    }

    public static void checkNull(Members member){
        if(member==null){
            throw new NullPointerException(NULL_POINTER_MEMBER);
        }
    }

    public static void checkNull(Boards board){
        if(board==null){
            throw new NullPointerException(NULL_POINTER_BOARD);
        }
    }

    public static void checkNull(WorkItems workItems){
        if(workItems==null){
            throw new NullPointerException(NULL_POINTER_WORKITEM);
        }
    }


    public static void checkFunctionalName(String name){
        if(name==null){
            throw new NullPointerException(NULL_POINTER_FUNCTIONAL_NAME);
        }
    }

    public static void checkTitle (String title){
        checkNull(title,NULL_POINTER_TITLE);
        if (title.length() < MIN_TITLE_VALUE || title.length() > MAX_TITLE_VALUE){
            throw new IllegalArgumentException(TITLE_INPUT_MESSAGE);
        }
    }

    public static void checkDescription(String description){
        checkNull(description,NULL_POINTER_DESCRIPTION);
        if (description.length() < MIN_DESCRIPTION_VALUE || description.length() > MAX_DESCRIPTION_VALUE){
            throw new IllegalArgumentException(DESCRIPTION_INPUT_MESSAGE);
        }
    }

    public static void checkRating(int rating){
        if(rating < MIN_RATING_VALUE || rating > MAX_RATING_VALUE){
            throw new IllegalArgumentException(RATING_INPUT_MESSAGE);
        }
    }

}
