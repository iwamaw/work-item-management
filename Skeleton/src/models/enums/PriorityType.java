package models.enums;

public enum PriorityType {
    HIGH,
    MAJOR,
    MINOR;

    @Override
    public String toString() {
        switch (this){
            case HIGH:
                return "High";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                throw new IllegalArgumentException();
        }
    }
}
