package models;

import models.contracts.Boards;
import models.contracts.Members;
import models.contracts.Teams;

import java.util.ArrayList;
import java.util.List;

public class TeamsImpl implements Teams {
    private static final String NAME_EMPTY_VALUE = "Name cannot be empty";

    private String name;
    private List<Members> members;
    private List<Boards> boards;

    public TeamsImpl(String name) {
        setName(name);
        members = new ArrayList<>();
        boards = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Members> getMembers() {
        return new ArrayList<>(members);
    }

    public List<Boards> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public void addBoardToTeam(Boards board) {
        ValidationHelper.checkNull(board);
        boards.add(board);
    }

    @Override
    public void addMemberToTeam(Members member) {
        ValidationHelper.checkNull(member);
        members.add(member);
    }

    @Override
    public String toString() {
        return String.format("Teams: %s", getName());
    }

    public String printHistory() {

        if (boards.size() == 0) {
            return String.format("History of team: %s%n" +
                    "No activity in this team", name);
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("HISTORY OF TEAM: %s%n", name));
        for (Boards board : boards) {
            stringBuilder.append(board.printHistory());
        }
        return stringBuilder.toString();

    }

    private void setName(String name) {
        ValidationHelper.checkNull(name, NAME_EMPTY_VALUE);
        this.name = name;
    }
}

