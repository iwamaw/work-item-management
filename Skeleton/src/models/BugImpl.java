package models;

import commands.list.AllStatusEnum;
import models.contracts.Bug;
import models.contracts.Members;
import models.enums.BugStatusType;
import models.enums.PriorityType;
import models.enums.SeverityType;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends WorkItemsImpl implements Bug {

    private BugStatusType status;
    private List<String> stepToReproduce;
    private PriorityType priority;
    private SeverityType severity;
    private Members assignee;

    public BugImpl(String title, String description) {
        super(title, description);
        stepToReproduce = new ArrayList<>();
        this.status = BugStatusType.ACTIVE;
        this.priority = PriorityType.MINOR;
        this.severity = SeverityType.MINOR;
    }

    public List<String> getStepToReproduce() {
        return new ArrayList<>(stepToReproduce);
    }

    public PriorityType getPriorityType() {
        return priority;
    }

    public SeverityType getSeverityType() {
        return severity;
    }

    public Members getAssignee() {
        return assignee;
    }

    public BugStatusType getStatusType() {
        return status;
    }

    public AllStatusEnum getGlobalStatus() {
        return AllStatusEnum.valueOf(getStatusType().toString().toUpperCase().replaceAll("\\s", ""));
    }

    public void setStep(String step) {
        stepToReproduce.add(step);
    }

    public void setPriorityType(PriorityType priority) {
        this.priority = priority;
    }

    public void setSeverity(SeverityType severity) {
        this.severity = severity;
    }

    public void setStatusType(BugStatusType status) {
        this.status = status;
    }

    public void setAssignee(Members assignee) {
        this.assignee = assignee;
    }

    @Override
    public String toString() {
        return String.format("%s Status: %s, Step to reproduce: %s, Priority: %s, Severity: %s, Assignee: %s%n"
                , super.toString(), getStatusType(), getStepToReproduce(), getPriorityType(), getSeverityType(), getAssignee());
    }

}
