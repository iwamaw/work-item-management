package models.contracts;

import models.enums.PriorityType;

public interface BugAndStory extends WorkItems {
    void setPriorityType(PriorityType priority);
    PriorityType getPriorityType();
    Members getAssignee();
}
