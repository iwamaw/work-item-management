package models.contracts;

import models.enums.PriorityType;
import models.enums.SizeType;
import models.enums.StoryStatusType;

public interface Story extends WorkItems,BugAndStory{

    void setSize(SizeType type);
    void setStatus(StoryStatusType status);
    SizeType getSizeType();
    StoryStatusType getStatusType();
    void setAssignee(Members members);
}
