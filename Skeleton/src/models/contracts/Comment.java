package models.contracts;

public interface Comment  {
    Members getName();
    String getMessage();
    String toString();
}
