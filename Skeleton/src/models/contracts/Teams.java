package models.contracts;

import models.BoardsImpl;
import models.MembersImpl;

import java.util.List;

public interface Teams {
    String getName();
    List<Members> getMembers();
    List<Boards> getBoards();
    void addBoardToTeam (Boards boardsTeam);
    void addMemberToTeam (Members membersTeam);
    String printHistory();
}
