package models.contracts;


import commands.list.AllStatusEnum;

import java.util.List;

public interface WorkItems {
    long getId();

    String getTitle();

    String getDescription();

    List<Comment> getComments();

    List<String> getHistory();

    void addCommentToWorkItem(Comment comment);

    AllStatusEnum getGlobalStatus();
}
